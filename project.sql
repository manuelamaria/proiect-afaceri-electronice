-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 16, 2019 at 10:09 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.0.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project`
--

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id_employee` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `department_role` varchar(500) DEFAULT NULL,
  `id_project` int(11) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id_employee`, `name`, `email`, `department_role`, `id_project`, `created_at`, `updated_at`) VALUES
(1, 'John Smith', 'john.smith@gmail.com', 'Technical Consultant', 6, '2018-12-30', '2018-12-30'),
(6, 'Manuela', 'manuela.burdusa@gmail.com', 'Dev', 4, '2019-01-13', '2019-01-13'),
(7, 'Test', 'Test', 'Test', NULL, '2019-01-16', '2019-01-16');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id_project` int(3) NOT NULL,
  `project_code` varchar(10) DEFAULT NULL,
  `project_name` varchar(300) DEFAULT NULL,
  `duration` varchar(1000) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id_project`, `project_code`, `project_name`, `duration`, `created_at`, `updated_at`) VALUES
(1, '123', 'Test ', '6 months', '2019-01-13', '2019-01-13'),
(2, '2000', 'Price&Cost', '12 months', '2019-01-13', '2019-01-13'),
(3, '444555', 'Taxes Application', '3 months', '2019-01-13', '2019-01-13'),
(4, '987223', 'SharePoint Dev', '9 months', '2019-01-13', '2019-01-13'),
(5, '2500', 'HolidaysApp', '2 months', '2019-01-13', '2019-01-13'),
(6, '1', '1', '1', '2019-01-13', '2019-01-13'),
(7, '12345', 'HR Management', '3 months', '2019-01-13', '2019-01-13');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `user_name` varchar(200) DEFAULT NULL,
  `user_email` varchar(300) DEFAULT NULL,
  `user_password` varchar(500) DEFAULT NULL,
  `role` varchar(300) DEFAULT NULL,
  `phone_number` varchar(50) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `user_name`, `user_email`, `user_password`, `role`, `phone_number`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', 'admin', 'Admin', '123456789', '2019-01-13', '2019-01-13'),
(2, 'Test', 'Test', 'admin', 'Test', 'Test', '2019-01-13', '2019-01-13'),
(3, 'John Smith', 'john@gmail.com', 'admin', 'Tech Consultant', '123456789', '2019-01-13', '2019-01-13'),
(4, 'Manuela Test', 'manuela.burdusa@gmail.com', 'admin', 'Full Stack Developer', '123456789', '2019-01-13', '2019-01-16'),
(5, 'Manuela Maria', 'admin', 'admin', 'Admin', '123', '2019-01-16', '2019-01-16'),
(6, 'USer', 'User@gmail.com', 'admin', 'HR Manager', '123456789', '2019-01-16', '2019-01-16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id_employee`),
  ADD KEY `id_project` (`id_project`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id_project`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id_employee` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id_project` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_ibfk_1` FOREIGN KEY (`id_project`) REFERENCES `projects` (`id_project`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
